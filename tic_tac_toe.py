#EM 1/5/2021
#Create a Tic Tac Toe game.
#2 players should be able to play the game (both sitting at the same computer)
#The board should be printed out every time a player makes a move
#You should be able to accept input of the player position
#and then place a symbol 'X' or 'O' on the board
#-----------------------------------------------------------------------------
from IPython.display import clear_output
import random
clear_output()

players = [0,'X','O']  # players[1] == 'X' and players[-1] == 'O'
# Global variables
init_board = {'7': ' ' , '8': ' ' , '9': ' ' ,\
              '4': ' ' , '5': ' ' , '6': ' ' ,\
              '1': ' ' , '2': ' ' , '3': ' ' }

init_avail = [str(num) for num in range(0,10)] # a List Comprehension
#-----------------------------------------------------------------------------
#Functions
#
def random_player():
    return random.choice((-1, 1))

def print_board(a,b):
    print('Available   TIC-TAC-TOE\n'+          '  moves\n\n  '+
          '{0:1}|{1:1}|{2:1}     {3:1}|{4:1}|{5:1}\n'\
          .format(a[7],a[8],a[9],b['7'],b['8'],b['9'])+
          '  -----     -----\n  '+
          '{0:1}|{1:1}|{2:1}     {3:1}|{4:1}|{5:1}\n'\
          .format(a[4],a[5],a[6],b['4'],b['5'],b['6'])+
          '  -----     -----\n  '+
          '{0:1}|{1:1}|{2:1}     {3:1}|{4:1}|{5:1}\n'\
          .format(a[1],a[2],a[3],b['1'],b['2'],b['3']))
    
  
def space_check(board,pos):
    return board[pos] == ' '

def full_board_check(board):
    return ' ' not in board.values()
    
def place_choise(board,mark):
    pos=''
    while pos not in board or not space_check(board, pos): 
        try:
            pos = input('Player %s, choose your next position: (1-9)\n '%(mark))
        except:
            print("I'm sorry, please try again.")        
    return pos

def place_marker(avail,board,marker,place):
    board[place] = marker
    avail[int(place)] = ''
        
def win_check(board,mark):
             # across the top 
    return ((board['7'] ==  board['8'] ==  board['9'] == mark) or
            ## across the middle
            (board['4'] ==  board['5'] ==  board['6'] == mark) or
            # across the bottom
            (board['1'] ==  board['2'] ==  board['3'] == mark) or
            # down the middle
            (board['7'] ==  board['4'] ==  board['1'] == mark) or
             # down the middle
            (board['8'] ==  board['5'] ==  board['2'] == mark) or
             # down the right side
            (board['9'] ==  board['6'] ==  board['3'] == mark) or
             # diagonal
            (board['7'] ==  board['5'] ==  board['3'] == mark) or
             # diagonal
            (board['9'] ==  board['5'] ==  board['1'] == mark))

def replay(): 
    return input('Do you want to play again? Enter Yes or No: '\
    ).lower().startswith('y')
#-----------------------------------------------------------------------------
theboard=init_board
available=init_avail
while True:
    clear_output()
    print('Welcome to Tic Tac Toe!')
    print('=========================')
    toggle = random_player()
    player = players[toggle]
    print('For this round, Player %s will go first!' %(player))
    game_on = True
    input('Hit Enter to start')
    while game_on:
        print_board(available,theboard)
        pos=place_choise(theboard,player)
        place_marker(available,theboard,player,pos)
        print_board(available,theboard)
        if win_check(theboard, player):
            print('Congratulations! Player '+player+' wins!')
            game_on = False
        else:
            if full_board_check(theboard):
                print_board(available,theboard)
                print('The game is a draw!')
                break
            else:
                toggle *= -1
                player = players[toggle]
                clear_output()
        
    # reset the board and available moves list
    theboard = init_board
    available = init_avail
    
    if not replay():
        break
